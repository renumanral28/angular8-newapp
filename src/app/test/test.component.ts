import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `
  <div>HELLO TCS</div>   

  <h2>Hey! {{name}}</h2>

  <h3>{{"Welcome"  + name }}</h3>

  <h3>{{"Length of name is " + name.length}}</h3>
  <h3>{{name.toUpperCase()}}</h3>
  <h3>{{greet()}}</h3>
  <h3>{{location}}</h3>
  `,
  styles: [`div{ 
    color:red ;
  }`]
})
export class TestComponent implements OnInit {

  public name="Rahul";
  public location=window.location.href;

  constructor() { }

  ngOnInit(): void {
  }

  greet(){
    return "hello " +this.name;
  }
}
